package com.app.avocare.viewholders;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.app.avocare.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FeedbackHolder extends RecyclerView.ViewHolder {

    public TextView Name;
    public TextView Email;
    public TextView Phone;
    public TextView Feedback;
    public Button SendCall;
    public Button SendEmail;

    public FeedbackHolder(@NonNull View itemView) {
        super(itemView);

        Name = itemView.findViewById(R.id.tv_name);
        Email = itemView.findViewById(R.id.tv_email);
        Phone = itemView.findViewById(R.id.tv_phone);
        Feedback = itemView.findViewById(R.id.tv_feedback);
        SendCall = itemView.findViewById(R.id.btn_call);
        SendEmail = itemView.findViewById(R.id.btn_email);
    }
}
